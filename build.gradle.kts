plugins {
    kotlin("multiplatform") version "1.7.10"
}

val artifactId = "ksoup-core"
group = "mixtape.oss.ksoup"
version = "1.0"

repositories {
    mavenCentral()
    maven("https://jitpack.io")
}

kotlin {
    explicitApi()

    jvm {
        compilations.all {
            kotlinOptions.jvmTarget = "11"
        }

        testRuns["test"].executionTask.configure {
            useJUnitPlatform()
        }
    }

    sourceSets["commonMain"].dependencies {
        implementation(kotlin("stdlib"))

        implementation("guru.zoroark.lixy:lixy:1849bb1")
        implementation("io.github.microutils:kotlin-logging-jvm:2.1.23")
    }

    sourceSets["commonTest"].dependencies {
        implementation(kotlin("test"))
    }

    sourceSets["jvmTest"].dependencies {
        implementation("org.jsoup:jsoup:1.15.2")
        implementation("ch.qos.logback:logback-classic:1.2.11")
    }
}
