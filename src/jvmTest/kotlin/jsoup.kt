import org.jsoup.Jsoup
import kotlin.time.ExperimentalTime
import kotlin.time.measureTimedValue

@OptIn(ExperimentalTime::class)
fun main() {
    val (html, took) = measureTimedValue {
        Jsoup.parse(HTML)
    }

    html.title()

    println("jsoup took: $took")
    println(html.select("body > h1").text())
}
