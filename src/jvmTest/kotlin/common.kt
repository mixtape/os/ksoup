import org.intellij.lang.annotations.Language
import org.jsoup.Connection
import org.jsoup.helper.HttpConnection

@Language("HTML")
val HTML = """
    <!doctype html>
    <html lang='en'>
      <head>
        <meta charset='UTF-8'>
        <meta name='viewport' content='width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0'>
        <meta http-equiv='X-UA-Compatible' content='ie=edge'>
        <title>Document</title>
      </head>
      <body>
        <h1 style='color: red;'>
          Hi, you're pretty cool! <!-- hi -->
          &gt;.&lt;
          <span test=0.3 another-test=word>uwu</span>
        </h1>
      </body>
    </html>
    """.trimIndent()

val HTML_LARGE by lazy {
    val connection = HttpConnection.connect("https://raw.githubusercontent.com/cheeriojs/cheerio/main/benchmark/documents/jquery.html")
    connection.method(Connection.Method.GET)
    connection.execute().body()
}

fun String.escapeNewlines(): String = replace("\n", "\\n")
