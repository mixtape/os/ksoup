import guru.zoroark.lixy.LixyNoMatchException
import mixtape.oss.ksoup.Ksoup
import mixtape.oss.ksoup.parser.ast.AstNode
import mixtape.oss.ksoup.parser.ast.AstParent

fun AstNode.asTree(indentation: String): String {
    val str = "$this".escapeNewlines()
    return indentation + when (this) {
        is AstParent -> if (children.isEmpty()) {
            str
        } else {
            "$str\n" + children.joinToString("\n") { it.asTree(indentation + "\t") }
        }

        else -> str
    }
}

fun main() {
    println(HTML_LARGE.substring(267, 267 + 10))

    try {
        val document = Ksoup.parse(HTML)

        println(document.children.joinToString("\n") { it.asTree("") })
        println(document.format())
    } catch (ex: LixyNoMatchException) {
        println("fuck: ${ex.message}")
    }
}
