package mixtape.oss.ksoup.parser

import mixtape.oss.ksoup.parser.ast.AstNode
import mixtape.oss.ksoup.parser.ast.node.*
import mixtape.oss.ksoup.parser.lexer.Token
import mixtape.oss.ksoup.parser.lexer.TokenType
import mixtape.oss.ksoup.tools.BasicParser
import mixtape.oss.ksoup.tools.Position
import mixtape.oss.ksoup.tools.VOID_ELEMENTS

internal fun BasicParser<AstNode>.parseAttribute(nameToken: Token? = next(TokenType.Word)): Attribute? {
    if (nameToken?.type != TokenType.Word) {
        return null
    }

    val attr = Attribute.Builder()
    attr.name = nameToken.value
    attr.position.start = nameToken.position.start

    attr.position.end = if (next(TokenType.Assignment) == null) {
        nameToken.position.end
    } else {
        val quote = next(TokenType.SingleQuote, TokenType.DoubleQuote)
        if (quote == null) {
            val content = require(TokenType.Number, TokenType.Word)
            attr.value = content.value
            content.position.end
        } else {
            attr.value = next(TokenType.QuotedContent)?.value ?: ""
            require(quote.type).position.end
        }
    }

    return attr.build()
}

internal fun BasicParser<AstNode>.parseTag(beginTag: Token? = next(TokenType.BeginTag)): Element.Tag? {
    if (beginTag?.type != TokenType.BeginTag) {
        return null
    }

    val tag = Element.Tag.Builder()
    tag.position.start = beginTag.position.start
    tag.leadingSlash = next(TokenType.Slash) != null
    tag.name = require(TokenType.Word).value

    skip(TokenType.Whitespace)

    if (tag.leadingSlash) {
        tag.position.end = require(TokenType.CloseTag).position.end
    } else {
        /* this is either a void tag or opening tag. */
        read { token ->
            when (token.type) {
                TokenType.CloseTag -> {
                    tag.position.end = token.position.end
                    false
                }

                TokenType.Slash -> {
                    // trailing slashes should always be followed by a tag end.
                    tag.position.end = require(TokenType.CloseTag).position.end
                    tag.trailingSlash = true
                    false
                }

                TokenType.Word -> {
                    tag.attributes += requireNotNull(parseAttribute(token)) { "Couldn't parse attribute" }
                    true
                }

                else -> true
            }
        }
    }

    return tag.build()
}

internal fun BasicParser<AstNode>.parseLiteral(vararg tokens: Token): Literal {
    val literal = Literal.Builder()
    literal.position.start = tokens.first().position.start
    literal.position.end = tokens.last().position.end

    val contentTokens = tokens.toList() + collect(TokenType.Whitespace, TokenType.Word, TokenType.Entity)
    if (contentTokens.isNotEmpty()) {
        literal.content = contentTokens.joinToString("") { it.value }
        literal.position.end = contentTokens.last().position.end
    }

    return literal.build()
}

internal fun BasicParser<AstNode>.parseComment(beginning: Token? = next(TokenType.CloseComment)): Comment? {
    if (beginning?.type != TokenType.BeginComment) {
        return null
    }

    val comment = Comment.Builder()
    comment.position.start = beginning.position.start
    comment.content = buildString {
        read {
            if (it.type == TokenType.CloseComment) {
                comment.position.end = it.position.end
                false
            } else {
                append(it.value)
                true
            }
        }
    }

    return comment.build()
}

internal fun BasicParser<AstNode>.parseDirective(beginning: Token? = next(TokenType.BeginDirective)): Directive? {
    if (beginning?.type != TokenType.BeginDirective) {
        return null
    }

    skip(TokenType.Whitespace)

    /* get directive name. */
    val name = require(TokenType.Word)
    skip(TokenType.Whitespace)

    /* get directive content. */
    val content = collect(TokenType.Word, TokenType.Whitespace).joinToString("") { it.value }

    return Directive(
        name.value,
        content,
        Position(beginning.position.start, require(TokenType.CloseTag).position.end)
    )
}

internal fun BasicParser<AstNode>.parseElement(openTag: Element.Tag): Element {
    val element = Element.Builder()
    element.openTag = openTag
    element.position.start = openTag.position.start

    if (openTag.name !in VOID_ELEMENTS) {
        read { token ->
            element.children += when (token.type) {
                /* read comment */
                TokenType.BeginComment ->
                    requireNotNull(parseComment(token)) { "Couldn't read comment." }

                /* read element */
                TokenType.BeginTag -> {
                    val tag = requireNotNull(parseTag(token)) {
                        "Couldn't read tag."
                    }

                    /* check for the closing tag of this element. */
                    if (tag.name == openTag.name && tag.leadingSlash) {
                        element.closeTag = tag
                        element.position.end = tag.position.end
                        return@read false
                    }

                    /* separate child element. */
                    parseElement(tag)
                }

                /* read `Literal` node */
                else -> parseLiteral(token)
            }

            true
        }
    } else {
        element.position.end = openTag.position.end
    }

    return element.build()
}

internal fun BasicParser<AstNode>.parseElement(beginTagToken: Token? = next(TokenType.BeginTag)): AstNode? {
    return parseElement(parseTag(beginTagToken) ?: return null)
}
