package mixtape.oss.ksoup.parser.lexer

import mixtape.oss.ksoup.tools.Position

public data class Token(
    public val value: String,
    public val type: TokenType,
    public val position: Position,
)
