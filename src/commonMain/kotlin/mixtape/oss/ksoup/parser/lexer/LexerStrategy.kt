package mixtape.oss.ksoup.parser.lexer

public interface LexerStrategy {
    /**
     * Tokenizes the supplied [content].
     *
     * @param content The string to tokenize.
     * @return The tokens
     */
    public fun tokenize(content: String): List<Token>
}
