package mixtape.oss.ksoup.parser.lexer.strategy

import guru.zoroark.lixy.LixyDslStateEnvironment
import guru.zoroark.lixy.lixy
import guru.zoroark.lixy.matchers.anyOf
import guru.zoroark.lixy.matchers.matches
import mixtape.oss.ksoup.parser.lexer.LexerState
import mixtape.oss.ksoup.parser.lexer.LexerStrategy
import mixtape.oss.ksoup.parser.lexer.Token
import mixtape.oss.ksoup.parser.lexer.TokenType
import mixtape.oss.ksoup.tools.Position

public object LixyLexerStrategy : LexerStrategy {
    private fun LixyDslStateEnvironment.matchNumber() {
        matches("[-+]?\\d*\\.?\\d+") isToken TokenType.Number
    }

    private fun LixyDslStateEnvironment.matchContent() {
        anyOf(" ", "\t", "\n") isToken TokenType.Whitespace

        matchNumber()
        matches("&\\w+;")          isToken TokenType.Entity
        matches("""[,.!)(\[\]}{/|\\:;'"\w-]+""") isToken TokenType.Word
//        +matcher { s, startAt ->
//            val word = s.substring(startAt, s.length)
//            LixyToken(word, startAt, startAt + word.length, LexerToken.Word)
//        }
    }

    private val lexer = lixy {
        default state {
            "<!--" isToken TokenType.BeginComment thenState LexerState.Comment
            "<!"   isToken TokenType.BeginDirective thenState LexerState.Tag
            "<"    isToken TokenType.BeginTag thenState LexerState.Tag

            matchContent()
        }

        LexerState.Comment state {
//            matches("""^(?!.*--!?>).*$""") isToken LexerToken.CommentContent
            matches("--!?>") isToken TokenType.CloseComment thenState default
            matchContent()
        }

        /* tag related shit */
        LexerState.Tag state {
            anyOf(" ", "\n").ignore

            ">" isToken TokenType.CloseTag thenState default
            "/" isToken TokenType.Slash
            "=" isToken TokenType.Assignment

            "'"  isToken TokenType.SingleQuote thenState LexerState.SingleQuote
            "\"" isToken TokenType.DoubleQuote thenState LexerState.DoubleQuote

            matchNumber()
            matches("[\\w-]+") isToken TokenType.Word
        }

        LexerState.SingleQuote state {
            "'" isToken TokenType.SingleQuote thenState LexerState.Tag
            matches("""(\\'|[^'])+""") isToken TokenType.QuotedContent
        }

        LexerState.DoubleQuote state {
            "\"" isToken TokenType.DoubleQuote thenState LexerState.Tag
            matches("""(\\"|[^"])+""") isToken TokenType.QuotedContent
        }
    }

    public override fun tokenize(content: String): List<Token> = lexer
        .tokenize(content)
        .map { Token(it.string, it.tokenType as TokenType, Position(it.startsAt, it.endsAt)) }
}
