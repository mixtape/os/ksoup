package mixtape.oss.ksoup.parser.lexer

import guru.zoroark.lixy.LixyToken
import guru.zoroark.lixy.LixyTokenType

public enum class TokenType(public val isQuote: Boolean = false) : LixyTokenType {
    BeginComment,   // "<!--"
    BeginDirective, // "<!"
    BeginTag,       // "<"

    CloseComment, // "-->"
    CloseTag,     // ">"

    CommentContent, // "-->"

    SingleQuote(true), // '
    DoubleQuote(true), // "

    QuotedContent,       //

    Slash,          // "/"

    Whitespace,    // " "
    Assignment,    // "="
    Word,          // "a-z"
    Number,        // "1", "1.0"
    Entity,        // "&[\w]+;"
    ;

    public companion object {
        public val LixyToken.type: TokenType get() = tokenType as TokenType
    }
}
