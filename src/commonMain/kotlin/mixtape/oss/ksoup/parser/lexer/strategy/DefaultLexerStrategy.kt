package mixtape.oss.ksoup.parser.lexer.strategy

import mixtape.oss.ksoup.parser.lexer.*

public object DefaultLexerStrategy : LexerStrategy {
    override fun tokenize(content: String): List<Token> {
        TODO("Not yet implemented")
    }

    public class Lexer(public val content: String) {
        /* lexer state */
        private val builders = mutableMapOf<TokenType, StringBuilder>()
        private var cursor = 0
        private var state: LexerState = LexerState.Default

        /* stuff */
        public val hitEof: Boolean
            get() = cursor !in content.indices

        public val remaining: String
            get() = content.substring(cursor..content.lastIndex)

        public fun tokenize(): List<Token> {
            return emptyList()
        }
    }
}
