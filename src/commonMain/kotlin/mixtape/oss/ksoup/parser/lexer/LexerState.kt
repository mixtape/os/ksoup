package mixtape.oss.ksoup.parser.lexer

import guru.zoroark.lixy.LixyStateLabel

public open class LexerState : LixyStateLabel {

    public object Comment : LexerState()

    public object SingleQuote : LexerState()

    public object DoubleQuote : LexerState()

    public object Tag : LexerState()

    public object Default : LexerState()
}
