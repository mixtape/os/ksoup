package mixtape.oss.ksoup.parser

import mixtape.oss.ksoup.parser.ast.AstNode
import mixtape.oss.ksoup.parser.lexer.LexerStrategy
import mixtape.oss.ksoup.parser.lexer.Token
import mixtape.oss.ksoup.parser.lexer.TokenType
import mixtape.oss.ksoup.parser.lexer.strategy.LixyLexerStrategy
import mixtape.oss.ksoup.tools.BasicParser
import mu.KotlinLogging
import kotlin.time.ExperimentalTime
import kotlin.time.measureTimedValue

public open class Parser(public val lexer: LexerStrategy = LixyLexerStrategy) {
    public companion object : Parser()

    private val log = KotlinLogging.logger { }

    /**
     *
     */
    public fun parse(html: String): List<AstNode> =
        parse(lexer.tokenize(html))

    /**
     *
     */
    @OptIn(ExperimentalTime::class)
    public fun parse(tokens: List<Token>): List<AstNode> {
        val parser = BasicParser<AstNode>(tokens)

        /* parse the rest of the tokens. */
        val (nodes, took) = measureTimedValue {
            /* parse the rest of the tokens. */
            parser.parse {
                val token = next()
                when (token.type) {
                    TokenType.BeginDirective ->
                        +requireNotNull(parseDirective(token)) { "Couldn't read directive." }

                    TokenType.BeginComment ->
                        +requireNotNull(parseComment(token)) { "Couldn't read comment." }

                    TokenType.BeginTag ->
                        +requireNotNull(parseElement(token)) { "Couldn't read element." }

                    else -> +parseLiteral(token)
                }
            }
        }

        log.debug { "Parser took $took" }
        return nodes
    }
}
