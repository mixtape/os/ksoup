package mixtape.oss.ksoup.parser.ast.node

import mixtape.oss.ksoup.parser.ast.AstNode
import mixtape.oss.ksoup.tools.Position
import mixtape.oss.ksoup.tools.Buildable

/*
 */
public data class Literal(
    val content: String,
    override val position: Position,
) : AstNode {
    public class Builder : Buildable<Literal> {
        public lateinit var content: String

        public val position: Position.Builder = Position.Builder()

        override fun build(): Literal = Literal(content, position.build())
    }

    override fun format(): String = content
}

