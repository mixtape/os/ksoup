package mixtape.oss.ksoup.parser.ast.node

import mixtape.oss.ksoup.parser.ast.AstNode
import mixtape.oss.ksoup.tools.Position

public data class Directive(
    val name: String,
    val content: String,
    override val position: Position,
) : AstNode {
    override fun format(): String = "<!$name $content>"
}

