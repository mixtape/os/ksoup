package mixtape.oss.ksoup.parser.ast.node

import mixtape.oss.ksoup.parser.ast.AstNode
import mixtape.oss.ksoup.tools.Position
import mixtape.oss.ksoup.tools.Buildable

public data class Comment(
    val content: String,
    override val position: Position,
) : AstNode {
    public class Builder : Buildable<Comment> {
        public val position: Position.Builder = Position.Builder()

        public lateinit var content: String

        override fun build(): Comment = Comment(content, position.build())
    }

    override fun format(): String = "<!--$content-->"
}

