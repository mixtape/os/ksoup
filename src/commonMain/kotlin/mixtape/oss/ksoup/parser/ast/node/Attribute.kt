package mixtape.oss.ksoup.parser.ast.node

import mixtape.oss.ksoup.parser.ast.AstNode
import mixtape.oss.ksoup.tools.Position
import mixtape.oss.ksoup.tools.Buildable

/**
 * An HTML element, usually a key-value pair separated by "=".
 */
public data class Attribute(
    val name: String,
    val value: String?,
    override val position: Position,
) : AstNode {
    public class Builder : Buildable<Attribute> {
        public lateinit var name: String

        public val position: Position.Builder = Position.Builder()

        public var value: String? = null

        override fun build(): Attribute = Attribute(name, value, position.build())
    }

    override fun format(): String = "$name${value?.let { "='$it'" }}"
}
