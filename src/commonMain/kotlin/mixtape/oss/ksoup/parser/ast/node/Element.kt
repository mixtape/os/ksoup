package mixtape.oss.ksoup.parser.ast.node

import mixtape.oss.ksoup.parser.ast.AstNode
import mixtape.oss.ksoup.parser.ast.AstParent
import mixtape.oss.ksoup.tools.Position
import mixtape.oss.ksoup.tools.Buildable

/**
 * An HTML element
 *
 * ## Example
 * ```html
 * <a href="link">content</a>
 * ```
 *
 * See also: https://en.wikipedia.org/wiki/HTML_element
 */
public data class Element(
    val openTag: Tag,
    val closeTag: Tag?,
    override val position: Position,
    override val children: List<AstNode>,
) : AstParent {
    public class Builder : Buildable<Element> {
        public lateinit var openTag: Tag

        public var closeTag: Tag? = null

        public val position: Position.Builder = Position.Builder()

        public var children: MutableList<AstNode> = mutableListOf()

        override fun build(): Element = Element(openTag, closeTag, position.build(), children)
    }

    public data class Tag(
        val name: String,
        val leadingSlash: Boolean = false,
        val trailingSlash: Boolean = false,
        val attributes: List<Attribute> = emptyList(),
        val position: Position,
    )  {
        public class Builder : Buildable<Tag> {
            public lateinit var name: String

            public val position: Position.Builder = Position.Builder()

            public var leadingSlash: Boolean = false

            public var trailingSlash: Boolean = false

            public var attributes: MutableList<Attribute> = mutableListOf()

            override fun build(): Tag = Tag(name, leadingSlash, trailingSlash, attributes, position.build())
        }

        public fun format(): String = buildString {
            append("<")
            if (leadingSlash) append("/")
            append(name)
            if (attributes.isNotEmpty()) {
                append(attributes.joinToString(" ", prefix = " ", postfix = "") { it.format() })
            }
            if (trailingSlash) append(" /")
            append(">")
        }
    }

    override fun format(): String = if (closeTag == null) {
        openTag.format()
    } else buildString {
        append(openTag.format())
        append(children.joinToString("") { it.format() })
        append(closeTag.format())
    }
}

