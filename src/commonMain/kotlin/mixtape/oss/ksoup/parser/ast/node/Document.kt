package mixtape.oss.ksoup.parser.ast.node

import mixtape.oss.ksoup.parser.ast.AstNode
import mixtape.oss.ksoup.parser.ast.AstParent
import mixtape.oss.ksoup.tools.Position
import mixtape.oss.ksoup.tools.Buildable

public data class Document(
    override val children: List<AstNode>,
    override val position: Position,
) : AstParent {
    public class Builder : Buildable<Document> {
        public val position: Position.Builder = Position.Builder()

        public var children: MutableList<AstNode> = mutableListOf()

        override fun build(): Document = Document(children, position.build())
    }
}
