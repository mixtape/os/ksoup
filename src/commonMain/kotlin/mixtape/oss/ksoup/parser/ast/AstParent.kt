package mixtape.oss.ksoup.parser.ast

/**
 * A node containing other nodes.
 */
public interface AstParent : AstNode {
    /**
     * The children of this node.
     */
    public val children: List<AstNode>

    override fun format(): String {
        return children.joinToString("") { it.format() }
    }
}
