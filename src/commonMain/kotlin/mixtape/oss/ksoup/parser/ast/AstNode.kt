package mixtape.oss.ksoup.parser.ast

import mixtape.oss.ksoup.tools.Position

public interface AstNode {
    /**
     * The position of this node in the source document.
     */
    public val position: Position

    /**
     * Formats this node.
     */
    public fun format(): String
}


