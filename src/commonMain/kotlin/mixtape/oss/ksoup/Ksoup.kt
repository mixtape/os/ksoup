package mixtape.oss.ksoup

import mixtape.oss.ksoup.parser.Parser
import mixtape.oss.ksoup.tools.Position
import mixtape.oss.ksoup.parser.ast.node.Document
import org.intellij.lang.annotations.Language

public object Ksoup {
    /**
     * Parses the supplied [html] and returns a new [Document].
     *
     * @param html The HTML to parse.
     */
    public fun parse(@Language("HTML") html: String): Document {
        val nodes = Parser.parse(html)
        return Document(
            nodes,
            Position(
                nodes.first().position.start,
                nodes.last().position.end
            )
        )
    }
}
