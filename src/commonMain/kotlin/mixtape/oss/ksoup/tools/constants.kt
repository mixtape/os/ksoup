package mixtape.oss.ksoup.tools

public val VOID_ELEMENTS: List<String> = listOf(
    "area",
    "base",
    "br",
    "col",
    "command",
    "embed",
    "hr",
    "img",
    "input",
    "keygen",
    "link",
    "meta",
    "param",
    "source",
    "track",
    "wbr"
)
