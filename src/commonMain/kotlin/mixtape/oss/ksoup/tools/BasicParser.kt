package mixtape.oss.ksoup.tools

import mixtape.oss.ksoup.parser.lexer.Token
import mixtape.oss.ksoup.parser.lexer.TokenType

public class BasicParser<NODE>(public val tokens: List<Token>) {
    private var mutCursor = 0
    private var nodes = mutableListOf<NODE>()

    public val cursor: Int
        get() = mutCursor

    public val hitEof: Boolean
        get() = mutCursor !in tokens.indices

    public fun parse(block: BasicParser<NODE>.() -> Unit): List<NODE> {
        while (!hitEof) {
            block()
        }

        return nodes
    }

    public fun read(block: (Token) -> Boolean) {
        while (!hitEof) {
            val token = next()
            if (!block(token)) {
                break
            }
        }
    }

    public fun oneOf(vararg parserBlocks: BasicParser<NODE>.() -> NODE?): NODE? {
        return parserBlocks.firstNotNullOfOrNull { it.invoke(this@BasicParser) }
    }

    public operator fun NODE.unaryPlus() {
        nodes.add(this)
    }

    public fun next(): Token = currentToken().also { mutCursor++ }

    public fun next(vararg types: TokenType): Token? = currentToken()
        .takeIf { it.type in types }
        ?.also { mutCursor++ }

    public fun collect(vararg types: TokenType): List<Token> {
        val tokens = mutableListOf<Token>()
        while (!hitEof) {
            val token = currentToken()
            if (token.type !in types) {
                break
            }

            tokens += token
            mutCursor++
        }

        return tokens
    }

    public fun skip(vararg types: TokenType) {
        while (!hitEof) {
            val token = currentToken()
            if (token.type !in types) {
                break
            }

            mutCursor++
        }
    }

    public fun require(vararg types: TokenType): Token {
        val token = currentToken()
        require(token.type in types) { "Token type ${token.type} is not one of: ${types.joinToString()}" }
        mutCursor++

        return token
    }

    private fun currentToken(): Token {
        require(!hitEof) { "Hit EOF" }
        return tokens[mutCursor]
    }
}
