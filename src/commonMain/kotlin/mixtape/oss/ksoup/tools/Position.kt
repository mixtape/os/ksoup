package mixtape.oss.ksoup.tools

import kotlin.properties.Delegates

/**
 * The position of a Node
 */
public data class Position(val start: Int, val end: Int) {
    public class Builder : Buildable<Position> {
        public var start: Int by Delegates.notNull()

        public var end: Int by Delegates.notNull()

        override fun build(): Position = Position(start, end)
    }
}
