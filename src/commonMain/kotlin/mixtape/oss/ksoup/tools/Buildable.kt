package mixtape.oss.ksoup.tools

public interface Buildable<T> {
    public fun build(): T
}
