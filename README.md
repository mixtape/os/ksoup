# 🔎 Ksoup

An HTML parser written in [**Kotlin**](https://kotlinlang.org) :D

- Pretty fast!
- Idk what else to put here because I haven't finished it.

## 🚀 Installation

### 🐘 Gradle

```kotlin
implementation("mixtape.oss.ksoup:ksoup-core:{VERSION}")
```

### 🪶 Maven

```xml
<repositories>
    <repository>
        <id>dimensional-maven</id>
        <name>Dimensional Maven</name>
        <url>https://maven.dimensional.fun/releases</url>
    </repository>
</repositories>

<dependencies>
    <dependency>
        <groupId>mixtape.oss.ksoup</groupId>
        <artifactId>ksoup-core</artifactId>
        <version>{VERSION}</version>
    </dependency>
</dependencies>
```

## 🥅 Usage

```kotlin
val document = Ksoup.parse("<!DOCTYPE html><html><head><title>Ksoup!</title></head></html>")

...
```

---

[Mixtape Bot](https://mixtape.systems) &copy; 2019 - 2022 All Rights Reserved.
